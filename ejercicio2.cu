#include <stdio.h>
#include <stdlib.h>


int dim = 8;

__global__  void multi (float* a, float *b, float *c, int dim) {

	//indice del thread
	int id = threadIdx.x + blockDim.x * blockIdx.x;
	float sum = 0;
    //calculo la sumatoria de los elementos de cada fila de la matriz b
		for (i = 0; i < dim; ++i) {
			sum += b[id][i];
		}

		a[id] = sum + c[id];

}
//multiplicacion ejercicio 2 capitulo 3
int main() {

	float h_a[dim]; // vector salida
	float h_b[dim][dim]; //matriz entrada
	float h_c[dim]; //vectoe entrada
	//punteros para el device
	float *d_a;
	float *d_b;
	float *d_c;
	//asigno memoria e inicializo en la memoria global del device
	cudaMalloc((void **) &d_a, dim*sizeof(float));	//vector
	cudaMalloc((void **) &d_b, dim*dim*sizeof(float)); //matriz
	cudaMalloc((void **) &d_c, dim*sizeof(float)); //vector
    //copio los datos del host al device, destino , origen
	cudaMemcpy(d_b, h_b, dim*dim*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_c, h_c, dim*sizeof(float), cudaMemcpyHostToDevice);
    
    //inicializo tamaños de la grilla y numero de threads
	dim3 dimGrid(1, 1); // 1x1 una sola dimension. un solo bloque
	dim3 dimBlock(dim, 1); dim*dim threads

	multi<<<dimGrid, dimBlock>>>(d_a, d_b, d_c, dim); //invoco al kernel
    //copio del device al host
	cudaMemcpy(h_a, d_a, dim*sizeof(float), cudaMemcpyDeviceToHost);
	//libero memoria asignada por malloc cuda
	cudaFree(d_a); 
	cudaFree(d_b); 
	cudaFree(d_c);

}