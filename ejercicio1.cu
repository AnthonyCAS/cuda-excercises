#include <stdio.h>
#include <stdlib.h>
//sumatoria ejercicio 1 capitulo 3 Anthony Ccapira

int dim = 100;

__global__  void multi (float* a, float *b, float *c, int dim) {

	//indice del thread
	int idf = threadIdx.x + blockDim.x * blockIdx.x;
	int idc = threadIdx.y + blockDim.y * blockIdx.y;
    //calculo la sumatoria de los elementos b+c en a

		a[idf][idc] = b[idf][idc] + c[idf][idc];

}
//sumatoria ejercicio 1 capitulo 3
int main() {

	float h_a[dim][dim]; // vector salida
	float h_b[dim][dim]; //matriz entrada
	float h_c[dim][dim]; //vectoe entrada
	//punteros para el device
	float *d_a;
	float *d_b;
	float *d_c;
	//asigno memoria e inicializo en la memoria global del device
	cudaMalloc((void **) &d_a, dim*dim*sizeof(float));	//vector
	cudaMalloc((void **) &d_b, dim*dim*sizeof(float)); //matriz
	cudaMalloc((void **) &d_c, dim*dim*sizeof(float)); //vector
    //copio los datos del host al device, destino , origen
	cudaMemcpy(d_b, h_b, dim*dim*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_c, h_c, dim*dim*sizeof(float), cudaMemcpyHostToDevice);
    
    //inicializo tama�os de la grilla y numero de threads
	dim3 dimGrid(1, 1); // 1x1 una sola dimension. un solo bloque
	dim3 dimBlock(dim, dim); dim*dim threads

	multi<<<dimGrid, dimBlock>>>(d_a, d_b, d_c, dim); //invoco al kernel
    //copio del device al host
	cudaMemcpy(h_a, d_a, dim*dim*sizeof(float), cudaMemcpyDeviceToHost);
	//libero memoria asignada por malloc cuda 

	//imprimir
	cudaFree(d_a); 
	cudaFree(d_b); 
	cudaFree(d_c);

}